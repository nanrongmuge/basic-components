package com.darwin.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OssComponentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OssComponentsApplication.class, args);
	}

}
