package com.darwin.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * 日期操作工具类
 *
 * @author yanghang47
 */
public class DateUtil {

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 返回当前的日期
     */
    public static LocalDate getCurrentLocalDate() {
        return LocalDate.now();
    }

    /**
     * 返回当前时间
     */
    public static LocalTime getCurrentLocalTime() {
        return LocalTime.now();
    }

    /**
     * 返回当前日期时间
     */
    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 获取当前的字符串格式的日期,格式为{@link DateUtil#DATETIME_FORMATTER}
     */
    public static String getCurrentDateStr() {
        return getCurrentLocalDate().format(DATE_FORMATTER);
    }

    /**
     * 获取当前的字符串格式的日期时间,格式为{@link DateUtil#DATETIME_FORMATTER}
     */
    public static String getCurrentDateTimeStr() {
        return getCurrentLocalDateTime().format(DATETIME_FORMATTER);
    }

    /**
     * 获取当前时间的毫秒数
     */
    public static Long toEpochMilli(LocalDateTime dateTime) {
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
