package com.darwin.redis.service.impl;

import com.darwin.redis.service.LeaderBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 排行榜实现类
 *
 * @author yanghang
 */
@Service
public class LeaderBoardServiceImpl<T> implements LeaderBoardService<T> {

    static final String TUPLE_VALUE = "value";
    static final String TUPLE_SCORE = "score";

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void batchAdd(String key, List<Map<String, Object>> maps) {
        if (CollectionUtils.isEmpty(maps)) {
            return;
        }
        Set<ZSetOperations.TypedTuple<Object>> collect = maps.stream().map(x -> new DefaultTypedTuple<>(x.get(TUPLE_VALUE), (Double) x.get(TUPLE_SCORE))).collect(Collectors.toSet());
        redisTemplate.opsForZSet().add(key, collect);
    }

    @Override
    public void add(String key, T value, Double score) {
        redisTemplate.opsForZSet().add(key, value, score);
    }

    @Override
    public List<T> listTopN(String key, int n) {
        Set<Object> objects = redisTemplate.opsForZSet().reverseRange(key, 0, n - 1);
        if (CollectionUtils.isEmpty(objects)) {
            return new ArrayList<>();
        }
        List<T> res = new ArrayList<>(objects.size());
        objects.forEach(x -> res.add((T) x));
        return res;
    }

    @Override
    public List<Map<String, Object>> listTopNWithScore(String key, int n) {
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(key, 0, n - 1);
        if (CollectionUtils.isEmpty(typedTuples)) {
            return new ArrayList<>();
        }
        List<Map<String, Object>> res = new ArrayList<>(typedTuples.size());
        typedTuples.forEach(x -> res.add(new HashMap<String, Object>(4) {{
            this.put(TUPLE_VALUE, x.getValue());
            this.put(TUPLE_SCORE, x.getScore());
        }}));
        return res;
    }

    @Override
    public Long countWithScore(String key, Double start, Double end) {
        return redisTemplate.opsForZSet().count(key, start, end);
    }

    @Override
    public Long count(String key) {
        return redisTemplate.opsForZSet().zCard(key);
    }

    @Override
    public Double incrementScore(String key, T t, Double variable) {
        return redisTemplate.opsForZSet().incrementScore(key, t, variable);
    }
}