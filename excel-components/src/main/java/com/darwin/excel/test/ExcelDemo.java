package com.darwin.excel.test;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;

/**
 * @author yanghang
 */
public class ExcelDemo {

    public static void main(String[] args) throws Exception {
        SXSSFWorkbook workbook = new SXSSFWorkbook();
        SXSSFSheet sheet = workbook.createSheet("sheet页1");
        Row titleRow = sheet.createRow(0);
        Cell titleRowCell = titleRow.createCell(0);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 1, 0, 7);
        sheet.addMergedRegion(cellRangeAddress);
        titleRowCell.setCellValue("这是一个标题");

        Row row = sheet.createRow(2);
        row.createCell(0).setCellValue("222");
        workbook.write(new FileOutputStream("E:\\owner-code\\basic-components\\excel-components\\a.xlsx"));
    }
}
