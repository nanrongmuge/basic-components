package com.darwin.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaComponentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaComponentsApplication.class, args);
    }

}
