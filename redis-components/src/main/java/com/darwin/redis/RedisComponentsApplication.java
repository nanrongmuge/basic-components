package com.darwin.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisComponentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisComponentsApplication.class, args);
    }

}
