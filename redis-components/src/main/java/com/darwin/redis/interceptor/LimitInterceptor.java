package com.darwin.redis.interceptor;

import com.darwin.redis.annotation.Limit;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

/**
 * 分布式限流过滤器
 *
 * @author yanghang
 */
@Aspect
@Configuration
@Slf4j
public class LimitInterceptor {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * lua脚本
     */
    private final String REDIS_SCRIPT = buildLuaScript();

    @Around("execution(public * *(..)) && @annotation(com.darwin.redis.annotation.Limit)")
    public Object interceptor(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        Limit limitAnnotation = method.getAnnotation(Limit.class);

        String key = limitAnnotation.key();
        int limitPeriod = limitAnnotation.period();
        int limitCount = limitAnnotation.count();
        String prefix = limitAnnotation.prefix();
        String param = StringUtils.isEmpty(prefix) ? key : prefix + ":" + key;
        List<String> keys = Collections.singletonList(param);
        RedisScript<Number> redisScript = new DefaultRedisScript<>(REDIS_SCRIPT, Number.class);
        Number count = redisTemplate.execute(redisScript, keys, limitCount, limitPeriod);
        if (count != null && count.intValue() <= limitCount) {
            return pjp.proceed();
        } else {
            throw new RuntimeException("网络出现了一点问题，，请稍后再试");
        }
    }

    /**
     * 限流 脚本
     *
     * @return lua脚本
     */
    private String buildLuaScript() {
        return "local c" +
                "\nc = redis.call('get', KEYS[1])" +
                // 调用不超过最大值，则直接返回
                "\nif c and tonumber(c) > tonumber(ARGV[1]) then" +
                "\nreturn c;" +
                "\nend" +
                // 执行计算器自加
                "\nc = redis.call('incr', KEYS[1])" +
                "\nif tonumber(c) == 1 then" +
                // 从第一次调用开始限流，设置对应键值的过期
                "\nredis.call('expire', KEYS[1], ARGV[2])" +
                "\nend" +
                "\nreturn c;";
    }
}
