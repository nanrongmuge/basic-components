package com.darwin.redis.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author yanghang
 */
@Component
public class RedisUtil {

    public static final String key = "SEQUENCE:" + LocalDate.now().toString();

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 获取序列，从1开始
     * <p>
     * 使用带有当前日期的key去redis中获取序列，每次获取加一之后的值，如果是当天第一次获取的话，那么调用increment
     * 方法之后会返回1，我们将这个key设置过期时间为1天，保证今天之前的key能被及时清理
     * </p>
     *
     * @param key 生成序列的key,带日期
     * @return 序列的值
     */
    public Long generateSequence(String key) {
        Long num = redisTemplate.opsForValue().increment(key);
        if (Objects.nonNull(num) && num.equals(1L)) {
            redisTemplate.expire(key, 1, TimeUnit.DAYS);
        }
        return num;
    }
}
