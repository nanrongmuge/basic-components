package com.darwin.redis.jedis;

import redis.clients.jedis.BinaryClient;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;

import java.util.HashMap;

/**
 * Jedis操作REDIS数据库
 *
 * @author yanghang
 */
public class JedisTest {

    public static void main(String[] args) {
        // 创建jedis客户端对象
        Jedis jedis = new Jedis("10.10.1.248", 26379);

        // jedis.auth("koala");

        // opsBase(jedis);

        // opsString(jedis);

        // opsList(jedis);

        // opsHash(jedis);

        // opsSet(jedis);

        // opsSortedSet(jedis);

         opsHyperLogLog(jedis);

        // opsGeo(jedis);

        // opsBitMaps(jedis);

        // 关闭jedis客户端对象
        jedis.close();
    }

    /**
     * 使用jedis的基本操作
     */
    private static void opsBase(Jedis jedis) {
        System.out.println("向redis中设置一个key " + jedis.set("key1", "value1"));
        System.out.println("向redis中设置一个key " + jedis.set("key2", "value2"));

        System.out.println("选择redis的一个数据库 " + jedis.select(0));
        System.out.println("查看当前数据库的大小 " + jedis.dbSize());
        System.out.println("查看redis中所有的key " + jedis.keys("*"));
        System.out.println("查看某个键对应值的数据类型 " + jedis.type("key1"));
        System.out.println("判断redis中是否存在一个key " + jedis.exists("key1"));
        System.out.println("设置redis中的一个key的过期时间 " + jedis.expire("key1", 10));
        System.out.println("查看redis中的这个key还有多长时间过期 " + jedis.ttl("key1"));
        System.out.println("删除redis中的一个键值对 " + jedis.del("key1"));
        System.out.println("清空当前的数据库 " + jedis.flushDB());
        System.out.println("清空所有的数据库 " + jedis.flushAll());
        System.out.println("将当前的数据库的一个键值对移动到另外一个数据库 " + jedis.move("key2", 1));
    }

    /**
     * 使用jedis操作string类型的数据
     */
    private static void opsString(Jedis jedis) {
        System.out.println("向redis中设置一个key " + jedis.set("key1", "value1"));
        System.out.println("只有当key不存在的时候再向redis中设置 " + jedis.setnx("key1", "value2"));
        System.out.println("向redis中设置一个key并设置过期时间 " + jedis.setex("key2", 10, "value2"));
        System.out.println("向redis中的某个key的值追加字符串 " + jedis.append("key1", "2"));
        System.out.println("获取redis中某个key对应的值 " + jedis.get("key1"));
        System.out.println("返回redis中某个key对应的值的长度大小 " + jedis.strlen("key1"));
        System.out.println("使用某个字符串从指定开始位置覆盖key对应的值到相同长度 " + jedis.setrange("key1", 0, "yh"));
        System.out.println("获取指定索引区间的字符串 " + jedis.getrange("key1", 0, 1));
        System.out.println("批量设置key和value " + jedis.mset("key3", "value3", "key4", "value4", "key5", "1"));
        System.out.println("批量获取redis中的值 " + jedis.mget("key3", "key4", "key5"));
        System.out.println("将redis中key对应的值加一 " + jedis.incr("key5"));
        System.out.println("将redis中key对应的值加指定的步长 " + jedis.incrBy("key5", 2));
        System.out.println("将redis中key对应的值减一 " + jedis.decr("key5"));
        System.out.println("将redis中key对应的值减指定的步长  " + jedis.decrBy("key5", 2));
    }

    /**
     * 使用jedis操作list类型的数据
     */
    private static void opsList(Jedis jedis) {
        System.out.println("从左边向集合中添加元素 " + jedis.lpush("list1", "value1"));
        System.out.println("从右边向集合中添加元素 " + jedis.rpush("list1", "value2"));
        System.out.println("从集合的左边弹出一个元素 " + jedis.lpop("list1"));
        System.out.println("从集合的右边弹出一个元素 " + jedis.rpop("list1"));
        System.out.println("从左边向集合中添加元素 " + jedis.lpush("list1", "value1"));
        System.out.println("从右边向集合中添加元素 " + jedis.rpush("list1", "value2"));
        System.out.println("查看集合中元素的数量 " + jedis.llen("list1"));
        System.out.println("覆盖集合中指定位置的key的值 " + jedis.lset("list1", 0, "value"));
        System.out.println("从后往前删除两个集合中指定值的元素 " + jedis.lrem("list1", -2, "value2"));
        System.out.println("获取集合中指定下标的元素 " + jedis.lindex("list1", 0));
        System.out.println("在制定元素之前或者之后插入一个元素" + jedis.linsert("list1", BinaryClient.LIST_POSITION.BEFORE, "value", "before"));
        System.out.println("列举集合中的所有元素 " + jedis.lrange("list1", 0, -1));
    }

    /**
     * 使用jedis操作hash类型的数据
     */
    private static void opsHash(Jedis jedis) {
        System.out.println("向redis中的key对应的map中put一个值 " + jedis.hset("map1", "key1", "value1"));
        System.out.println("向redis中的key对应的map中put多个值 " + jedis.hmset("map1", new HashMap<String,String>(8) {
            {
                this.put("key2", "value2");
                this.put("key3", "value3");
                this.put("key4", "1");
            }
        }));
        System.out.println("获取redis中key对应map的某个值 " + jedis.hget("map1", "key1"));
        System.out.println("获取redis中key对应map的多个值 " + jedis.hmget("map1", "key1", "key2"));
        System.out.println("获取redis中key对应map的键值对数量 " + jedis.hlen("map1"));
        System.out.println("获取redis中key对应map的所有的key的value的值 " + jedis.hgetAll("map1"));
        System.out.println("获取redis中key对应map的所有的键 " + jedis.hkeys("map1"));
        System.out.println("获取redis中key对应map的所有的值 " + jedis.hvals("map1"));
        System.out.println("如果map对应的key不存在才设置 " + jedis.hsetnx("map1","key4", "2"));
        System.out.println("给map中的某个key对应的值增长对应的值 " + jedis.hincrBy("map1", "key4", 2));
    }

    /**
     * 使用jedis操作set类型的数据
     */
    private static void opsSet(Jedis jedis) {
        System.out.println("向set中添加一个或多个元素 " + jedis.sadd("set1", "value1", "value2", "value3", "value4", "value5", "value6"));
        System.out.println("获得set中的元素个数 " + jedis.scard("set1"));
        System.out.println("判断set中是否存在某个元素 " + jedis.sismember("set1", "value1"));
        System.out.println("获得set中的所有元素 " + jedis.smembers("set1"));
        System.out.println("删除set中的一个或者多个元素 " + jedis.srem("set1", "value5", "value6"));
        System.out.println("随机弹出set中的某个元素 " + jedis.spop("set1"));

        System.out.println("向set中添加一个或多个元素 " + jedis.sadd("set2", "value1", "value2"));

        System.out.println("获得两个set的差集 " + jedis.sdiff("set1", "set2"));
        System.out.println("获得两个set的差集并转储到另外一个set中 " + jedis.sdiffstore("set3", "set1", "set2"));
        System.out.println("获得两个set的交集 " + jedis.sinter("set1", "set2"));
        System.out.println("获得两个set的交集并转储到另外一个set中 " + jedis.sinterstore("set3", "set1", "set2"));
        System.out.println("获得两个set的并集 " + jedis.sunion("set1", "set2"));
        System.out.println("获得两个set的并集并转储到另外一个set中 " + jedis.sunionstore("set3", "set1", "set2"));
        System.out.println("获得set中的所有元素 " + jedis.smembers("set3"));
    }

    /**
     * 使用jedis操作zset类型的数据
     * 带分数输出值的时候返回的是字节数组
     */
    private static void opsSortedSet(Jedis jedis) {
        System.out.println("向zset中添加一个元素 " + jedis.zadd("sortedSet1",10,"value"));
        System.out.println("向zset中添加多个元素 " + jedis.zadd("sortedSet1", new HashMap<String, Double>(8) {
            {
                this.put("value1", 20D);
                this.put("value2", 30D);
                this.put("value3", 40D);
                this.put("value4", 50D);
            }
        }));
        System.out.println("统计zset中的元素个数 " + jedis.zcard("sortedSet1"));
        System.out.println("修改zset中某个元素的分数 " + jedis.zincrby("sortedSet1",-10D,"value"));
        System.out.println("删除zset中的一个或多个元素 " + jedis.zrem("sortedSet1", "value3", "value4"));
        System.out.println("正序获取zset中指定索引范围内的元素 " + jedis.zrange("sortedSet1", 0, -1));
        System.out.println("正序获取zset中指定索引范围内的元素及元素的分数" + jedis.zrangeWithScores("sortedSet1", 0, -1));
        System.out.println("正序获取zset中指定分数范围内的元素 " + jedis.zrangeByScore("sortedSet1", 0, 40));
        System.out.println("正序获取zset中指定分数范围内的元素及元素的分数" + jedis.zrangeWithScores("sortedSet1", 0, 40));
        System.out.println("逆序获取zset中指定索引范围内的元素 " + jedis.zrevrange("sortedSet1", 0, -1));
        System.out.println("逆序获取zset中指定索引范围内的元素及元素的分数" + jedis.zrevrangeWithScores("sortedSet1", 0, -1));
        System.out.println("逆序获取zset中指定分数范围内的元素 " + jedis.zrevrangeByScore("sortedSet1", 40, 0));
        System.out.println("逆序获取zset中指定分数范围内的元素及元素的分数" + jedis.zrevrangeByScoreWithScores("sortedSet1", 40, 0));
    }

    /**
     * 使用jedis测试基数数据类型
     */
    private static void opsHyperLogLog(Jedis jedis) {
        System.out.println("向一个数据组中添加数据 " + jedis.pfadd("hyperLogLog", "a", "b", "c", "d", "e", "f", "a", "a", "b"));
        System.out.println("向一个数据组中添加数据 " + jedis.pfadd("hyperLogLog1", "a", "b", "c", "d"));
        jedis.pfmerge("hyperLogLog0","hyperLogLog", "hyperLogLog1");
        System.out.println("输出指定key的基数是多少 " + jedis.pfcount("hyperLogLog0"));
    }

    /**
     * 使用jedis操作geospatial类型的数据
     */
    private static void opsGeo(Jedis jedis) {
        System.out.println("向geo中添加地图位置信息 " + jedis.geoadd("geo1", 116.404269d, 39.91582d, "beijing"));
        System.out.println("向geo中添加地图位置信息 " + jedis.geoadd("geo1", 121.478799d, 31.235456d, "shanghai"));

        System.out.println("返回某几个地点的地理位置信息 " + jedis.geopos("geo1","beijing", "shanghai"));
        System.out.println("返回两个地点之间的直线距离 " + jedis.geodist("geo1", "beijing", "shanghai"));
        System.out.println("返回指定经纬度多少距离以内的元素 " + jedis.georadius("geo1", 116.404269d, 39.91582d,1500, GeoUnit.KM));
        System.out.println("返回指定元素多少距离以内的元素 " + jedis.georadiusByMember("geo1", "beijing",1500, GeoUnit.KM));
    }

    /**
     * 使用jedis操作bitmaps类型的数据
     */
    private static void opsBitMaps(Jedis jedis) {
        System.out.println("向bit中设置数据" + jedis.setbit("bit1",0,true));
        System.out.println("向bit中设置数据" + jedis.setbit("bit1",5,false));
        System.out.println("向bit中设置数据" + jedis.setbit("bit1",10,false));

        System.out.println("获取指定索引上的数字 " + jedis.getbit("bit1",4));
        System.out.println("返回bitmaps上共有多少true " + jedis.bitcount("bit1"));
    }
}