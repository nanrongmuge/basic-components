package com.darwin.redis.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis操作底层封装实现类
 *
 * @author yanghang
 */
@Component
@Slf4j
public class RedisDao {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    // =======================common=======================

    /**
     * 获取redis当前数据库中的所有key
     *
     * @return 当前redis库中的所有key, 如果没有的话返回空set
     */
    public Set<String> allKeys() {
        return keys("*");
    }

    /**
     * 获取redis当前数据库中的正则表达式匹配上的key
     *
     * @return redis当前数据库中的正则表达式匹配上的key, 如果没有的话返回空set
     */
    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 判断当前数据库是否包含key
     *
     * @param key 键
     * @return true-包含,false-不包含
     */
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 给一个key设置过期时间，单位为秒
     *
     * @param key  键
     * @param time 过期时间，单位为秒
     * @return true设置成功, false-数据库中不存在设置过期时间的key
     */
    public Boolean setExpire(String key, Long time) {
        return redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    /**
     * 根据key获取过期时间
     *
     * @param key 键
     * @return 剩余过期的时间(如果这个key没有过期时间返回 - 1, 如果根本就没有这个key返回 - 2)
     */
    public Long getExpireTime(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 移除一个key的过期时间
     *
     * @param key 键
     * @return 是否移除成功(如果根本就没有这个key或者这个key不会过期返回false, 如果这个key有过期时间并成功移除才返回true)
     */
    public Boolean persist(String key) {
        return redisTemplate.boundValueOps(key).persist();
    }

    /**
     * 将一个键值对,移动到指定索引的数据库中
     *
     * @param key   键
     * @param index 数据库索引
     * @return 是否移动成功(如果键存在且移动到的数据库不是当前库返回true, 如果键不存在且移动到的数据库不是当前库返回false)
     * @throws RuntimeException 如果移动到的库和当前是一个库会抛异常
     */
    public Boolean move(String key, Integer index) {
        try {
            return redisTemplate.move(key, index);
        } catch (RedisSystemException exception) {
            throw new RuntimeException("请确认你要移动到的库不是当前数据库");
        }
    }

    /**
     * 将一个键值对,从数据库中移除
     *
     * @param key 键
     * @return 是否移除 true-redis中中存在这个key成功移除,false-redis中不存在这个key
     */
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }


    // =======================string=======================

    /**
     * 放入String类型的缓存
     *
     * @param key   键
     * @param value 值,如果是数字类型的字符,这里要直接传入数值型
     */
    public void setValue(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 获取对应键的值
     *
     * @param key 键,如果这个键就返回空字符串
     */
    public String getValue(String key) {
        return Optional.ofNullable(redisTemplate.opsForValue().get(key)).map(String::valueOf).orElse("");
    }

    /**
     * 向一个字符串追加字符
     *
     * @param key        主键,主键不存在,会直接把追加的文本当做第一次设置的值
     * @param appendText 追加的文本
     * @return 追加后的文本的长度
     */
    public Integer append(String key, String appendText) {
        return redisTemplate.opsForValue().append(key, appendText);
    }

    /**
     * 操作数值类型的key对应的value加一
     *
     * @param key 键
     * @return 改变完的值
     */
    public Long increment(String key) {
        try {
            return redisTemplate.opsForValue().increment(key);
        } catch (Exception e) {
            throw new RuntimeException("你要变动的值不是数字类型");
        }
    }

    /**
     * 操作数值类型的key对应的value增长指定的值
     *
     * @param key 键
     * @param num 要改变的值的大小
     * @return 改变完的值
     */
    public Long increment(String key, Long num) {
        try {
            return redisTemplate.opsForValue().increment(key, num);
        } catch (RedisSystemException e) {
            throw new RuntimeException("你要变动的值不是数字类型");
        }
    }

    /**
     * 操作数值类型的key对应的value减一
     *
     * @param key 键
     * @return 改变完的值
     */
    public Long decrement(String key) {
        try {
            return redisTemplate.opsForValue().decrement(key);
        } catch (RedisSystemException e) {
            throw new RuntimeException("你要变动的值不是数字类型");
        }
    }

    /**
     * 操作数值类型的key对应的value减少指定的值
     *
     * @param key 键
     * @param num 要改变的值的大小
     * @return 改变完的值
     */
    public Long decrement(String key, Long num) {
        try {
            return redisTemplate.opsForValue().decrement(key, num);
        } catch (RedisSystemException e) {
            throw new RuntimeException("你要变动的值不是数字类型");
        }
    }
}

