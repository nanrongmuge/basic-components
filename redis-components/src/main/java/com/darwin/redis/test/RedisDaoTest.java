package com.darwin.redis.test;

import com.darwin.redis.dao.RedisDao;
import com.darwin.redis.service.LeaderBoardService;
import com.darwin.redis.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试RedisDao
 *
 * @author yanghang
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisDaoTest {

    @Autowired
    private RedisDao redisDao;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private LeaderBoardService<String> leaderBoardService;

    @Test
    public void test() {
        redisDao.setValue("22212", 11);
        System.out.println(redisDao.decrement("22212", 12L));
    }

    @Test
    public void test2() {
        new Thread(() -> {
            for (int j = 0; j < 1000; j++) {
                System.out.println(redisUtil.generateSequence(RedisUtil.key));
            }
        }).start();
    }

    @Test
    public void test3() {
        final String RANK_NAME = "rank";
        leaderBoardService.add(RANK_NAME, "张三", 20D);
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("value", "杨航" + i);
            map.put("score", (double) i);
            list.add(map);
        }
        leaderBoardService.batchAdd(RANK_NAME, list);
        System.out.println("leaderBoardService.count(RANK_NAME) = " + leaderBoardService.count(RANK_NAME));
        System.out.println("leaderBoardService.countWithScore(RANK_NAME,0d,20D) = " + leaderBoardService.countWithScore(RANK_NAME, 0d, 20D));
        System.out.println(leaderBoardService.listTopN(RANK_NAME, 5));
        System.out.println(leaderBoardService.listTopNWithScore(RANK_NAME, 5));
        System.out.println("leaderBoardService.incrementScore(RANK_NAME,\"杨航0\",11D) = " + leaderBoardService.incrementScore(RANK_NAME, "杨航0", 11D));
    }
}
