package com.darwin.patterns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesignPatternsComponentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesignPatternsComponentsApplication.class, args);
	}

}
