package com.darwin.utils;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class BloomFilterTest {

    public static void main(String[] args) {
        int error = 0;
        BloomFilter<String> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), 100000,0.1);

        Set<String> rightSet = new HashSet<>();
        Set<String> errorSet = new HashSet<>();
        for (int i = 0; i < 100000; i++) {
            String s = UUID.randomUUID().toString();
            if (i % 2 == 1) {
                rightSet.add(s);
                bloomFilter.put(s);
            } else {
                errorSet.add(s);
            }

        }
        for (String s: rightSet) {
            if (!bloomFilter.mightContain(s)) {
                error++;
            }
        }
        for (String s: errorSet) {
            if (bloomFilter.mightContain(s)) {
                error++;
            }
        }
        System.out.println("错误的个数是" + error);
    }
}
