package com.darwin.patterns.create;

import java.util.Objects;

/**
 * 单例设计模式
 */
public class SingletonPatternDemo {

    public static void main(String[] args) {
    }
}

/**
 * 饿汉
 */
class Singleton1 {

    private static final Singleton1 singleton1 = new Singleton1();

    private Singleton1() {
    }

    public static Singleton1 getInstance() {
        return singleton1;
    }
}

/**
 * 懒汉，线程不安全
 */
class Singleton2 {
    private static Singleton2 singleton2;

    private Singleton2() {
    }

    public static Singleton2 getInstance() {
        if (Objects.isNull(singleton2)) {
            singleton2 = new Singleton2();
        }
        return singleton2;
    }
}

/**
 * 懒汉，线程不安全
 */
class Singleton3 {
    private static Singleton3 singleton3;

    private Singleton3() {
    }

    public static synchronized Singleton3 getInstance() {
        if (Objects.isNull(singleton3)) {
            singleton3 = new Singleton3();
        }
        return singleton3;
    }
}

/**
 * DCL双重检查
 */
class Singleton4 {
    private static Singleton4 singleton4;

    private Singleton4() {
    }

    public static Singleton4 getInstance() {
        if (Objects.isNull(singleton4)) {
            synchronized (Singleton4.class) {
                if (Objects.isNull(singleton4)) {
                    singleton4 = new Singleton4();
                }
            }
        }
        return singleton4;
    }
}

/**
 * 登记式/静态内部类
 */
class Singleton5 {
    private static class SingletonHolder {
        private static final Singleton5 INSTANCE = new Singleton5();
    }

    private Singleton5() {
    }

    public static Singleton5 getInstance() {
        return Singleton5.SingletonHolder.INSTANCE;
    }
}