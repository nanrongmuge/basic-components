package com.darwin.redis.controller;

import com.darwin.redis.annotation.Limit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yanghang
 */
@RestController
@RequestMapping("/api/v1/test")
public class TestLimitController {

    @GetMapping("limit")
    @Limit(key = "test", period = 1, count = 10, prefix = "YH")
    public String test() {
        System.out.println("访问成功");
        return "ok";
    }

}
