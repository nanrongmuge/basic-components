package com.darwin.oss.test;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.Bucket;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.GetObjectRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class BucketTest {

    // Endpoint以杭州为例，其它Region请按实际情况填写。
    static String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
    // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
    static String accessKeyId = "LTAI4GFtXcER8q14WtQeNXSK";
    static String accessKeySecret = "R38SUqleJxwNgd5MZhiBy0OuSyvc0V";

    public static void main(String[] args) throws FileNotFoundException {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

//        createBucket(ossClient);
//        listBuckets(ossClient);
//        putObject(ossClient);
        getObject(ossClient);
        // 关闭OSSClient。
        ossClient.shutdown();
    }

    private static void listBuckets(OSS ossClient) {
        List<Bucket> buckets = ossClient.listBuckets();
        for (Bucket bucket : buckets) {
            System.out.println(" - " + bucket.getName());
        }
    }

    private static void createBucket(OSS ossClient) {
        // 创建CreateBucketRequest对象。
        CreateBucketRequest createBucketRequest = new CreateBucketRequest("yanghang2");

        // 如果创建存储空间的同时需要指定存储类型以及数据容灾类型, 可以参考以下代码。
        // 此处以设置存储空间的存储类型为标准存储为例。
        // createBucketRequest.setStorageClass(StorageClass.Standard);
        // 默认情况下，数据容灾类型为本地冗余存储，即DataRedundancyType.LRS。如果需要设置数据容灾类型为同城冗余存储，请替换为DataRedundancyType.ZRS。
        // createBucketRequest.setDataRedundancyType(DataRedundancyType.ZRS)

        // 创建存储空间。
        ossClient.createBucket(createBucketRequest);
    }

    private static void putObject(OSS ossClient) throws FileNotFoundException {
        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Pic\\lyf.jpg");
        ossClient.putObject("yanghang2", "lyf", inputStream);
    }

    private static void getObject(OSS ossClient) {
        ossClient.getObject(new GetObjectRequest("yanghang2", "lyf"), new File("C:\\Pic\\lyf2.xlsx"));
    }

}
