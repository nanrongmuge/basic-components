package com.darwin.redis.service;

import java.util.List;
import java.util.Map;

/**
 * 排行榜接口类
 *
 * @author yanghang
 */
public interface LeaderBoardService<T> {

    /**
     * 向排行榜中批量添加元素
     *
     * @param key     排行榜的键
     * @param mapList map中需要包含value、score两个字段
     */
    void batchAdd(String key, List<Map<String, Object>> mapList);

    /**
     * 单个添加元素
     *
     * @param key   排行榜的键
     * @param value 元素的名称
     * @param score 元素的分数
     */
    void add(String key, T value, Double score);

    /**
     * 获取前N名,获取结果只包含value的值
     *
     * @param key 排行榜的键
     * @param n   获取分数最高的多少人
     * @return 前N名的value值
     */
    List<T> listTopN(String key, int n);

    /**
     * 获取前N名,获取结果包含value的值和score的值
     *
     * @param key 排行榜的键
     * @param n   获取分数最高的多少人
     * @return 前N名的value值
     */
    List<Map<String, Object>> listTopNWithScore(String key, int n);

    /**
     * 获取分数区间内的元素的个数
     *
     * @param key   排行榜的键
     * @param start 分数区间,开始分数
     * @param end   分数区间,结束分数
     * @return 元素数量
     */
    Long countWithScore(String key, Double start, Double end);

    /**
     * 获取整个排行榜的元素数量
     *
     * @param key 排行榜的键
     * @return 所有的元素数量
     */
    Long count(String key);

    /**
     * 增加一个元素的分数
     *
     * @param key      排行榜的键
     * @param t        需要操作的元素名称
     * @param variable 需要增长的分数
     * @return 增长完的分数
     */
    Double incrementScore(String key, T t, Double variable);
}
