package com.darwin.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsComponentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsComponentsApplication.class, args);
    }

}
