package com.darwin.sms.service.impl;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.darwin.sms.service.SendSmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 发送短信业务实现类
 *
 * @author yanghang
 */
@Service
public class SendSmsServiceImpl implements SendSmsService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void sendSms(String phoneNumbers) {
        if (StringUtils.isEmpty(phoneNumbers) || !isPhoneNumbers(phoneNumbers)) {
            throw new RuntimeException("请输入正确格式的手机号");
        }
        String phoneKey = "PHONE:CHECK:" + phoneNumbers;
        Boolean isExpire = redisTemplate.hasKey(phoneKey);
        if (Objects.nonNull(isExpire) && isExpire) {
            throw new RuntimeException("请" + redisTemplate.getExpire(phoneKey) + "s后再试");
        }

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GFtXcER8q14WtQeNXSK", "R38SUqleJxwNgd5MZhiBy0OuSyvc0V");
        IAcsClient client = new DefaultAcsClient(profile);

        String code = getRandomFourNum();

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        request.putQueryParameter("SignName", "杨航说java");
        request.putQueryParameter("TemplateCode", "SMS_205391791");
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");
        try {
            long startTime = System.currentTimeMillis();
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            System.out.println((System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            e.printStackTrace();
        }
        String phoneCodeKey = "PHONE:CODE:" + phoneNumbers;
        // 将生成的验证码存到redis中,5分钟过期
        redisTemplate.opsForValue().set(phoneCodeKey, code, 5, TimeUnit.MINUTES);
        // 同一个手机号一分钟只能申请一个验证码,防止恶意调用
        redisTemplate.opsForValue().set(phoneKey, 1, 1, TimeUnit.MINUTES);
    }

    public boolean verificationCode(String phoneNumbers, String code) {
        if (StringUtils.isEmpty(phoneNumbers) || !isPhoneNumbers(phoneNumbers)) {
            throw new RuntimeException("请输入正确格式的手机号");
        }
        if (StringUtils.isEmpty(code)) {
            throw new RuntimeException("请输入正确格式的验证码");
        }

        String phoneCodeKey = "PHONE:CODE:" + phoneNumbers;
        Object o = redisTemplate.opsForValue().get(phoneCodeKey);
        if (Objects.isNull(o)) {
            return false;
        } else {
            String s = o.toString();
            return code.equals(s);
        }
    }

    /**
     * 正则表达式,校验一个号码是不是手机号
     *
     * @param mobiles 电话号码
     * @return true--是正确的手机号,false--不是正确的手机号格式
     */
    public static boolean isPhoneNumbers(String mobiles) {
        String regExp = "^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(166)|(17[3,5,6,7,8])" +
                "|(18[0-9])|(19[8,9]))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(mobiles);
        return m.matches();
    }

    /**
     * 随机生成4位数的验证码
     *
     * @return 长度为4的验证码
     */
    public static String getRandomFourNum() {
        String[] beforeShuffle = new String[]{"2", "3", "4", "5", "6", "7",
                "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z"};
        List<String> list = Arrays.asList(beforeShuffle);
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s);
        }
        String afterShuffle = sb.toString();
        return afterShuffle.substring(5, 9);
    }
}