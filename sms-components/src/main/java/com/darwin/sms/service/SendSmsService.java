package com.darwin.sms.service;

public interface SendSmsService {

    /**
     * 发送验证码短信
     *
     * @param phoneNumbers 接受的电话
     */
    void sendSms(String phoneNumbers);

    /**
     * 校验手机号的验证码
     *
     * @param phoneNumbers 手机号
     * @param code         验证码
     * @return ture--校验通过,false--校验不通过
     */
    boolean verificationCode(String phoneNumbers, String code);
}
