package com.darwin.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelComponentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelComponentsApplication.class, args);
    }

}
