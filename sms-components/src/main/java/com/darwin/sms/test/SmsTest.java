package com.darwin.sms.test;

import com.darwin.sms.service.SendSmsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yanghang
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsTest {

    @Autowired
    private SendSmsService sendSmsService;

    @Test
    public void test() {
        sendSmsService.sendSms("18301205109");
        // System.out.println(sendSmsService.verificationCode("18301205109", "9R68"));
    }
}