package com.darwin.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsComponentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsComponentsApplication.class, args);
    }

}
